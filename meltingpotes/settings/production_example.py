from .base import *

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgresql',
        'USER': 'hosting-db',
        'PASSWORD': '',
        'HOST': 'localhost',
    }
}

try:
    from .local import *
except ImportError:
    pass
