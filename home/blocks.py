from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock


class SectionBlock(blocks.StructBlock):

    title = blocks.CharBlock()
    text = blocks.RichTextBlock()
    image = ImageChooserBlock()
    page_title = blocks.CharBlock(required=False)
    page = blocks.PageChooserBlock(required=False)
    image_position = blocks.ChoiceBlock((
        ('left', 'Gauche'),
        ('right', 'Droite'),
    ))


    class Meta:
        icon = 'doc-full'
        template = 'blocks/section.html'


class MealBlock(blocks.StructBlock):

    meal = blocks.PageChooserBlock(target_model='home.MealPage', required=True)
    image_position = blocks.ChoiceBlock((
        ('left', 'Gauche'),
        ('right', 'Droite'),
    ))


    class Meta:
        icon = 'doc-full'
        template = 'blocks/meal.html'


# deprecated
class MealCategoryBlock(blocks.PageChooserBlock):

    target_model = 'home.MealCategoryPage'

    class Meta:
        template = 'blocks/meal_category.html'



class HeadingBlock(blocks.CharBlock):

    class Meta:
        icon = 'title'
        template = 'blocks/heading.html'
        classname = 'heading_input struct-block'


class TitleSectionBlock(blocks.StructBlock):

    title = blocks.CharBlock()
    intro = blocks.CharBlock()

    class Meta:
        icon = 'title'
        template = 'blocks/title_section.html'
        classname = 'heading2_input struct-block'


class H2Block(blocks.CharBlock):

    class Meta:
        icon = 'title'
        template = 'blocks/h2.html'
        classname = 'heading2_input struct-block'



class H3Block(blocks.CharBlock):

    class Meta:
        icon = 'title'
        template = 'blocks/h3.html'
        classname = 'heading3_input struct-block'


class BlockQuoteBlock(blocks.StructBlock):

    text = blocks.RichTextBlock()
    credit = blocks.RichTextBlock(required=False)

    class Meta:
        icon = 'openquote'
        template = 'blocks/blockquote.html'
        classname = 'blockquote_input struct-block'


class IllustratedParagraphBlock(blocks.StructBlock):

    text = blocks.RichTextBlock()
    image = ImageChooserBlock()
    image_position = blocks.ChoiceBlock((
        ('left', 'Float left'),
        ('right', 'Float right'),
    ))

    class Meta:
        icon = 'doc-full'
        template = 'blocks/illustrated_paragraph.html'


class NavigationBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    anchor = blocks.CharBlock(required=False)
    page = blocks.PageChooserBlock(required=False)

    class Meta:
        icon = 'arrow-right'
        template = 'blocks/navigation.html'


class ContactInfoBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    name = blocks.CharBlock(required=False)
    url = blocks.URLBlock(required=False)

    class Meta:
        icon = 'link'
        template = 'blocks/contact_info.html'


class ContactIconBlock(blocks.StructBlock):
    title = blocks.CharBlock(required=False)
    icon = blocks.CharBlock()
    url = blocks.URLBlock(required=False)

    class Meta:
        icon = 'image'
        template = 'blocks/contact_icon.html'


class ContactIconListBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    icons = blocks.ListBlock(ContactIconBlock())

    class Meta:
        icon = 'mail'
        template = 'blocks/contact_icon_list.html'

class MealListBlock(blocks.StructBlock):

    title = blocks.CharBlock()
    meals = blocks.ListBlock(
        blocks.PageChooserBlock(page_type='home.MealPage'),
        required=True)

    class Meta:
        icon = 'doc-full'
        template = 'blocks/meal_list.html'


class LinkBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    url = blocks.CharBlock()

    class Meta:
        icon = 'link'
        template = 'link.html'


class LinkListBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    links = blocks.StreamBlock([
        ('link', LinkBlock()),
        ('page', blocks.PageChooserBlock()),
    ])

    class Meta:
        icon = 'link'
        template = 'blocks/link_list.html'


class IngredientBlock(blocks.StructBlock):
    ingredient = blocks.CharBlock()
    is_allergenic = blocks.BooleanBlock(required=False)
    allergene = blocks.CharBlock(required=False)

    class Meta:
        icon = 'doc'
        template = 'blocks/ingredient.html'
