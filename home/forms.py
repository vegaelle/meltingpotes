from django import forms
from django.forms.renderers import DjangoTemplates
from wagtail.contrib.forms import forms as wt_forms


class FormBuilder(wt_forms.FormBuilder):

    def create_multiline_field(self, field, options):
        return forms.CharField(**options)

    def get_field_options(self, field):
        widget_types = {'singleline': forms.TextInput,
                        'multiline': forms.Textarea,
                        'date': forms.DateInput,
                        'datetime': forms.DateTimeInput,
                        'email': forms.EmailInput,
                        'url': forms.URLInput,
                        'number': forms.NumberInput,
                        }
        opts = super().get_field_options(field)
        if hasattr(field, 'css_class') and field.css_class != ''\
                and field.field_type in widget_types:
            if 'widget' not in opts:
                opts['widget'] = widget_types[field.field_type]()
            opts['widget'].attrs.update({'class': field.css_class})
        if 'placeholder' not in opts:
            if 'widget' not in opts:
                opts['widget'] = widget_types[field.field_type]()
            opts['widget'].attrs.update({'placeholder': field.label})
        return opts


class CustomRenderer(DjangoTemplates):

    def render(self, template_name, context, request=None):
        if template_name.endswith('checkbox_select.html'):
            template_name = 'home/forms/widgets/checkbox_select.html'
        return super().render(template_name=template_name, context=context,
                              request=request)
