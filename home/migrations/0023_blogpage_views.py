# Generated by Django 2.1.8 on 2019-07-03 17:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0022_blogcategorypage_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogpage',
            name='views',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
