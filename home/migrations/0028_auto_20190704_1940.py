# Generated by Django 2.1.8 on 2019-07-04 17:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0027_auto_20190704_1909'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogcategorypage',
            name='collection',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailcore.Collection'),
        ),
        migrations.AlterField(
            model_name='blogindexpage',
            name='collection',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailcore.Collection'),
        ),
        migrations.AlterField(
            model_name='homepage',
            name='collection',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailcore.Collection'),
        ),
        migrations.AlterField(
            model_name='mealcategorypage',
            name='collection',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailcore.Collection'),
        ),
        migrations.AlterField(
            model_name='menucategorypage',
            name='collection',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailcore.Collection'),
        ),
        migrations.AlterField(
            model_name='menupage',
            name='collection',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailcore.Collection'),
        ),
    ]
