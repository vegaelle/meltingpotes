# Generated by Django 2.0.13 on 2019-04-24 13:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0040_page_draft_title'),
        ('home', '0006_homepage_logo'),
    ]

    operations = [
        migrations.AddField(
            model_name='standardpage',
            name='collection',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='wagtailcore.Collection'),
        ),
    ]
