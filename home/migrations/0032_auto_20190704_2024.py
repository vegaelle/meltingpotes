# Generated by Django 2.1.8 on 2019-07-04 18:24

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0031_auto_20190704_2022'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homepage',
            name='contact_details',
            field=wagtail.core.fields.StreamField([('icon', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock(required=False)), ('icon', wagtail.core.blocks.CharBlock()), ('url', wagtail.core.blocks.URLBlock(required=False))]))], blank=True),
        ),
    ]
