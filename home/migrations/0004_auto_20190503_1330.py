# Generated by Django 2.1.8 on 2019-05-03 11:30

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0003_auto_20190502_1619'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mealpage',
            name='ingredients',
            field=wagtail.core.fields.StreamField([('ingredients', wagtail.core.blocks.ListBlock(wagtail.core.blocks.CharBlock()))], blank=True),
        ),
    ]
