Django>=2.1,<2.2
wagtail>=2.5,<2.6
wagtail-2fa==1.1.0
wagtail-markdown==0.5
psycopg2>=2.8,<2.9
